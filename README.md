## Description

This project is for any gitlab-ce contributors, who want to track his contribution process.

## How to start

Just [add new issue](https://gitlab.com/innerwhisper/contributors-den/issues/new) with your name in title, and choose "01-beginner" template for issue description.