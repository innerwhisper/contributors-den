## Purpose of my contribution

(you can choose any of proposed, or suggest yours)
- get my first experience in development
- to chat with good people
- to be in list of contributors - http://contributors.gitlab.com/
- make gitlab-ce better product
- to became MVP and get golden fork - https://about.gitlab.com/mvp/
- it need for my current job (e.g. new features, when you are using gitlab)

## Preparation process
- [ ] Take a first look at https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md
- [ ] Take a first look at https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md
- [ ] Take a first look at [Definition of done](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#definition-of-done). Definitely, it's the most important guide for you.
- [ ] Make a first documentation-related contribution from MR to  - (put your issue/MR link here)
  - [ ] Find appropriate issue here https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_id=0&label_name[]=Accepting%20Merge%20Requests&label_name[]=Documentation

## Contributing process
- [ ] Install gdk - https://gitlab.com/gitlab-org/gitlab-development-kit
- [ ] Take first bug issue 
- [ ] Take first refactoring issue

## Your first Merge Request

- [ ] Step 1
- [ ] Step 2

## Your first Issue

- [ ] Step 1
- [ ] Step 2

## Your contribution's progress
- [ ] Any issue created - (put your issue number here)
- [ ] Any merge request merged - (put your MR number here)
- [ ] Catch a bug - (put your issue here)
